#pragma unmanaged

#include "APIController.h"


#include <cstdlib>
#include <restbed>
#include <memory>
#include <json_spirit/json_spirit.h>
#include <iostream>
#include "DataBase.h"

#include <thread>
#include <iostream>

using namespace std;
using namespace restbed;
using namespace json_spirit;

DataBase* _data;



void post_method_handler(const shared_ptr< Session > session)
{
	const auto request = session->get_request();

	int content_length = request->get_header("Content-Length", 0);

	session->fetch(content_length, [](const shared_ptr< Session > session, const Bytes & body)
	{
		fprintf(stdout, "%.*s\n", (int)body.size(), body.data());
		char str[sizeof(body.data())];
		memcpy(str, body.data(), sizeof(body.data()));
		json_spirit::Value obj;
		json_spirit::read(str, obj);
		auto arr = obj.get_array();
		cout << arr[0].get_str();

		session->close(OK, "Hello, World!", { { "Content-Length", "13" } });
	});
}

void get_method_handler(const shared_ptr< Session > session)
{
	const auto request = session->get_request();

	int content_length = request->get_header("Content-Length", 0);

	session->fetch(content_length, [](const shared_ptr< Session > session, const Bytes & body)
	{
		fprintf(stdout, "%.*s\n", (int)body.size(), body.data());
		char str[sizeof(body.data())];
		memcpy(str, body.data(), sizeof(body.data()));
		json_spirit::Value obj;
		json_spirit::read(str, obj);
		auto arr = obj.get_array();
		cout << arr[0].get_str();

		session->close(OK, "Hello, World!", { { "Content-Length", "13" } });
	});
}

void login_handler(const shared_ptr< Session > session)
{
	const auto request = session->get_request();

	int content_length = request->get_header("Content-Length", 0);

	session->fetch(content_length, [](const shared_ptr< Session > session, const Bytes & body)
	{

		string str = restbed::String::to_string(body);
		cout << " request login " << str << endl;
		json_spirit::Value value;
		json_spirit::read(str, value);
		auto obj = value.get_obj();
		string id, pwd, token;

		json_spirit::Object response;

		for (auto entry : obj)
		{
			if (entry.name_ == "id")
			{
				id = entry.value_.get_str();
			}
			else
				if (entry.name_ == "pwd")
				{
					pwd = entry.value_.get_str();
				}
		}
		if (_data->check_user(id, pwd, token))
		{
			response.push_back(json_spirit::Pair("token", token));
			response.push_back(json_spirit::Pair("status_code", 0));
			response.push_back(json_spirit::Pair("message", "Login successful!"));
			session->close(OK, json_spirit::write(response), { {"Server","" } });
		}
		else
		{
			response.push_back(json_spirit::Pair("token", ""));
			response.push_back(json_spirit::Pair("status_code", 999));
			response.push_back(json_spirit::Pair("message", "Login fail, check id or password!"));
			session->close(OK, json_spirit::write(response), { {"Server","" } });
		}
	});
}

void register_handler(const shared_ptr< Session > session)
{
	const auto request = session->get_request();

	int content_length = request->get_header("Content-Length", 0);

	session->fetch(content_length, [](const shared_ptr< Session > session, const Bytes & body)
	{
		string str = restbed::String::to_string(body);
		json_spirit::Value value;
		json_spirit::read(str, value);
		auto obj = value.get_obj();
		string id, pwd;
		for (auto entry : obj)
		{
			if (entry.name_ == "id")
			{
				id = entry.value_.get_str();
			}
			else
				if (entry.name_ == "pwd")
				{
					pwd = entry.value_.get_str();
				}
		}
		json_spirit::Object response;
		string token = "";
		if (_data->add_new(id, pwd, token))
		{
			cout << id << "register successful" << endl;
			response.push_back(json_spirit::Pair("token", token));
			response.push_back(json_spirit::Pair("status_code", 0));
			response.push_back(json_spirit::Pair("message", "Register successful!"));
			session->close(OK, json_spirit::write(response), { {"Server","" } });
		}
		else
		{
			cout << id << "register success" << endl;
			response.push_back(json_spirit::Pair("token", ""));
			response.push_back(json_spirit::Pair("status_code", 999));
			response.push_back(json_spirit::Pair("message", "ID exist"));
			session->close(OK, json_spirit::write(response), { {"Server","" } });
		}

	});
}

void check_username_handler(const shared_ptr< Session > session)
{
	const auto request = session->get_request();

	int content_length = request->get_header("Content-Length", 0);
	std::cout << "get user name " << endl;

	session->fetch(content_length, [](const shared_ptr< Session > session, const Bytes & body)
	{
		string str = restbed::String::to_string(body);
		json_spirit::Value value;
		json_spirit::read(str, value);
		auto obj = value.get_obj();
		string token;
		for (auto entry : obj)
		{
			std::cout << "entry name " << entry.name_ << endl;
			std::cout << "entry value " << entry.value_.get_str() << endl;
			if (entry.name_ == "token")
			{
				token = entry.value_.get_str();
				std::cout << "get user name " << token<<endl;
				break;
			}
		}
		json_spirit::Object response;
		string username = "";
		if (_data->get_username(token, username))
		{
			std::cout << "token valid" << endl;
			response.push_back(json_spirit::Pair("id", username));
			response.push_back(json_spirit::Pair("status_code", 0));
			response.push_back(json_spirit::Pair("message", "Register successful!"));
			session->close(OK, json_spirit::write(response), { {"Server","" } });
		}
		else
		{
			response.push_back(json_spirit::Pair("id", ""));
			response.push_back(json_spirit::Pair("status_code", 999));
			response.push_back(json_spirit::Pair("message", "ID exist"));
			session->close(OK, json_spirit::write(response), { {"Server","" } });
		}

	});
}

shared_ptr<Resource> login_request()
{
	auto resource = make_shared< Resource >();
	resource->set_path("/api/login");
	resource->set_method_handler("POST", login_handler);
	return resource;
}

shared_ptr<Resource> register_request()
{
	auto resource = make_shared< Resource >();
	resource->set_path("/api/register");
	resource->set_method_handler("POST", register_handler);
	return resource;
}

shared_ptr<Resource> get_username_request()
{
	auto resource = make_shared< Resource >();
	resource->set_path("/api/get-username");
	resource->set_method_handler("POST", check_username_handler);
	return resource;
}

void start_server()
{
	_data = new DataBase();

	auto settings = make_shared< Settings >();
	settings->set_port(8765);
	settings->set_worker_limit(6);
	settings->set_default_header("Connection", "close");

	Service service;
	service.publish(login_request());
	service.publish(register_request());
	service.publish(get_username_request());
	service.start(settings);
}

void start()
{
	start_server();
}
