#include "pch.h"
#include "DataBase.h"
#include <iostream>
#include <fstream>
#include "json_spirit/json_spirit.h"
#include <ctime>

using namespace std;


const string file_name = "user.txt";

json_spirit::Object _data;
string gen_random(const int len)
{
	srand((unsigned)time(NULL));
	static const char alphanum[] =
		"0123456789"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz";
	std::string tmp_s;
	tmp_s.reserve(len);

	for (int i = 0; i < len; ++i) {
		tmp_s += alphanum[rand() % (sizeof(alphanum) - 1)];
	}

	return tmp_s;
}

DataBase::DataBase()
{
	load();
}
DataBase::~DataBase()
{

}

void DataBase::load()
{
	ifstream user_r(file_name);

	if (user_r.fail())
	{
		ofstream user_w(file_name);
		user_w << "{}";
		user_w.close();
		user_r.open(file_name);
	}

	json_spirit::Value value;
	json_spirit::read(user_r, value);
	_data = value.get_obj();
	user_r.close();

}
bool DataBase::add_new(string id, string pwd, string &token)
{
	for (auto entry : _data)
	{
		if (entry.name_ == id)
		{
			return false;
		}
	}
	json_spirit::Object n_user;
	n_user.push_back(json_spirit::Pair("pwd", pwd));
	token = gen_random(256);
	n_user.push_back(json_spirit::Pair("token", token));

	_data.push_back(json_spirit::Pair(id, n_user));
	ofstream user_w(file_name);
	//user_w.clear();
	json_spirit::write(_data, user_w);

	return true;
}
bool DataBase::get_username(string token, string &username)
{
	for (auto entry : _data)
	{
		auto user = entry.value_.get_obj();

		bool valid = false;

		for (auto info : user)
		{
			if (info.name_ == "token")
			{
				if (info.value_ == token)
				{
					valid = true;
					username = entry.name_;
					return valid;
				}
			}
		}
	}
	return false;
}

bool DataBase::check_user(string id, string pwd, string &token)
{
	for (auto entry : _data)
	{
		if (entry.name_ == id)
		{
			bool valid = false;
			auto user = entry.value_.get_obj();
			for (auto info : user)
			{
				if (info.name_ == "pwd")
				{
					if (info.value_ == pwd)
					{
						valid = true;
					}
				}
				else if (info.name_ == "token")
				{
					token = info.value_.get_str();
				}
			}
			return valid;
		}
	}
	return false;
}



