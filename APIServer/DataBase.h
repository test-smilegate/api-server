#pragma once
#include <iostream>
#include <string>

using namespace std;
class DataBase
{
private:
	void load();

public: DataBase();
		~DataBase();
		bool add_new(string id, string pwd, string &token);
		bool check_user(string id, string pws, string &token);
		bool get_username(string token, string &username);
};

